Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: vim-airline
Source: https://github.com/vim-airline/vim-airline

Files: *
Copyright: 2013-2019 Bailey Ling et al <bling@live.ca>
           2014-2019 Mathias Andersson et al
	   2013-2019 Bjorn Neergaard, w0rp, hallettj et al
           2018 mox et al
License: expat

Files: autoload/airline/extensions/ycm.vim
Copyright: 2015 Evgeny Firsov
License: expat

Files: autoload/airline/extensions/capslock.vim
Copyright: 2014 Mathias Andersson
License: expat

Files: autoload/airline/extensions/tabline/ctrlspace.vim
Copyright: 2016 Kevin Sapper
License: expat

Files: autoload/airline/extensions/tabline/formatters/tabnr.vim
Copyright: 2017-2018 C.Brabandt et al
License: expat

Files: autoload/airline/extensions/ale.vim
Copyright: 2013-2018 Bjorn Neergaard, w0rp et al.
License: expat

Files: autoload/airline/extensions/vimagit.vim
Copyright: 2016-2018 Jerome Reybert et al.
License: expat

Files: autoload/airline/extensions/xbklayout.vim
Copyright: 2017-2018 YoungHoon Rhiu et al.
License: expat

Files: autoload/airline/extensions/async.vim
Copyright: 2013-2018 C.Brabandt et al.
License: expat

Files: autoload/airline/extensions/cursormode.vim
Copyright: 2014 Andrea Cedraro <a.cedraro@gmail.com>
           2017 Eduardo Suarez-Santana <e.suarezsantana@gmail.com>
License: expat

Files: autoload/airline/extensions/denite.vim
Copyright: 2017-2018 Thomas Dy et al.
License: expat

Files: autoload/airline/extensions/fugitiveline.vim
Copyright: 2017-2018 Cimbali et al
License: expat

Files: autoload/airline/extensions/keymap.vim
Copyright: 2013-2018 Doron Behar, C.Brabandt et al.
License: expat

Files: debian/*
Copyright: 2017-2019 Jonathan Carter <jcc@debian.org>
License: expat

License: expat
 Permission is hereby granted, free of charge, to any person obtaining
 a copy of this software and associated documentation files (the "Software"),
 to deal in the Software without restriction, including without limitation
 the rights to use, copy, modify, merge, publish, distribute, sublicense,
 and/or sell copies of the Software, and to permit persons to whom the
 Software is furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included
 in all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
 OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
